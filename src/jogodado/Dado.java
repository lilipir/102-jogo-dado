package jogodado;

public class Dado {
	private int lados;

	public Dado(int lados) {
		this.lados = lados;
	}

	public int getLados() {
		return lados;
	}

	public int sortear() {
		double resultado = Math.random() * lados;
		return (int) Math.ceil(resultado);
	}
}
