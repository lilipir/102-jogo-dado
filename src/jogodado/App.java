package jogodado;

public class App {
	public static void main(String[] args) {
		Dado dado = new Dado(6);
		
		Resultado resultado = Sorteador.sortear(dado, 3);
		
		Impressora.imprimir(resultado);
	}
}
