package jogodado;

public class Sorteador {
	public static Resultado sortear(Dado dado, int vezes) {
		int[] valores = new int[vezes];
		
		for(int i = 0; i < vezes; i ++) {
			valores[i] = dado.sortear();
		}
		
		return new Resultado(valores);
	}
}
