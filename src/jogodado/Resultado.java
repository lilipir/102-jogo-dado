package jogodado;

import java.util.Arrays;

public class Resultado {
	private int[] valores;
	
	public Resultado(int[ ] valores) {
		this.valores = valores;
	}
	
	public int somar() {
		int acumulador = 0;
		
		for(int valor: valores) {
			acumulador += valor;
		}
		
		return acumulador;
	}
	
	@Override
	public String toString() {
		String retorno = Arrays.toString(valores);
		retorno = retorno.replace("[", "");
		retorno = retorno.replace("]", "");
		
		return retorno + " - " + somar();
	}
}
